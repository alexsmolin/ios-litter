//
//  HomeViewController.swift
//  litter
//
//  Created by Леха Кирон on 17.06.2018.
//  Copyright © 2018 nerox. All rights reserved.
//

import UIKit
import CoreLocation

class HomeViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, CLLocationManagerDelegate {
    var isAccuracyLocation: Bool = false
    let locationManager = CLLocationManager()
    var image: UIImage!
    var lat: String!
    var lon: String!
    var course: String!
    var speed: String!
    var altitude: String!
    var horizontal_accuracy: String!
    @IBOutlet weak var photoButton: UIButton!
    @IBOutlet weak var horizontalAccuracyLabel: UILabel!
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        //  Скрыть Navigation Bar в этом контроллере
//        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        //  Скрыть Navigation Bar в этом контроллере
//        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Отменить цвет кнопки при selected
        photoButton.adjustsImageWhenHighlighted = false
        
        //  Радиус кнопки
        photoButton.layer.cornerRadius = 5

        
        //  locationManager.requestAlwaysAuthorization()
        if CLLocationManager.locationServicesEnabled(){
            locationManager.delegate = self
            locationManager.requestWhenInUseAuthorization()
            //устанавливаю точность
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            
            //  запуск GPS
            locationManager.startUpdatingLocation()
            
            //  Запуск компапса
            locationManager.startUpdatingHeading()
        }
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        locationManager.stopUpdatingLocation()
    }
    
    //  При нажатии на кнопку камера
    @IBAction func openCameraButton(_ sender: UIButton) {
        if !isAccuracyLocation {
            let actionAlert = UIAlertAction(title: "Ок", style: .cancel, handler: nil)
            let alert = UIAlertController(title: "Ошибка", message: "Точность GPS не должна привышать ±\(AppContstants.minHorizontalAccuracy) метров! Повторите действие на открытом месте.", preferredStyle: .alert)
            alert.addAction(actionAlert)
            self.present(alert, animated: true, completion: nil)
        }
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
            imagePicker.allowsEditing = false
            //  запустить камеру
            self.present(imagePicker, animated: true, completion: nil)
        }
        
    }
    
    @available(iOS 2.0, *)
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        image = imageOrientation((info[UIImagePickerControllerOriginalImage] as? UIImage)!)
        image = resizeImage(image: image)
        lat = String(locationManager.location!.coordinate.latitude)
        lon = String(locationManager.location!.coordinate.longitude)
        course = String(locationManager.heading!.trueHeading)
        speed = String(locationManager.location!.speed)
        altitude = String(locationManager.location!.altitude)
        horizontal_accuracy = String(locationManager.location!.horizontalAccuracy)
        
        //  закрыть камеру
        self.dismiss(animated: true, completion: nil)
        
        print("photo")
        
        //  переход на другой контроллер
        performSegue(withIdentifier: "showForm", sender: self)
    }
    
    //  передать значения в другой контроллер во время перехода
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showForm" {
            if let destinationVC = segue.destination as? ViewController {
                destinationVC.image = image
                destinationVC.lat = lat
                destinationVC.lon = lon
                destinationVC.course = course
                destinationVC.speed = speed
                destinationVC.altitude = altitude
                destinationVC.horizontal_accuracy = horizontal_accuracy
            }
        }
    }
    
    //  При изменение авторизации
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if (status == CLAuthorizationStatus.denied) {
            showLocationDisabledPopUp()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        //        horizontalAccuracyLabel.text = String(newHeading.trueHeading)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        //        horizontalAccuracyLabel.text = String(locationManager.location!.horizontalAccuracy)
        isAccuracyLocation = false
        if let last = locations.last {
            horizontalAccuracyLabel.text = "Точность GPS: ±\(Int(last.horizontalAccuracy)) метров"
            if(Int(last.horizontalAccuracy) <= AppContstants.minHorizontalAccuracy) {
                isAccuracyLocation = true
            }
        } else {
            horizontalAccuracyLabel.text = "Точность GPS не известна!"
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
    
    //  Открывать окно с информацией что локация запрещена
    func showLocationDisabledPopUp() {
        let alertController = UIAlertController(title: "Локация отключена!", message: "Нужно включить локацию", preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Отменить", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        let openAction = UIAlertAction(title: "Открыть настройки", style: .default) { (action) in
            if let url = URL(string: UIApplicationOpenSettingsURLString) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
        alertController.addAction(openAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    //  Определяпт ориентацию фотографии и переворачивает ее
    func imageOrientation(_ src:UIImage) -> UIImage {
        if src.imageOrientation == UIImageOrientation.up {
            return src
        }
        var transform: CGAffineTransform = CGAffineTransform.identity
        switch src.imageOrientation {
        case UIImageOrientation.down, UIImageOrientation.downMirrored:
            transform = transform.translatedBy(x: src.size.width, y: src.size.height)
            transform = transform.rotated(by: CGFloat(Double.pi))
            break
        case UIImageOrientation.left, UIImageOrientation.leftMirrored:
            transform = transform.translatedBy(x: src.size.width, y: 0)
            transform = transform.rotated(by: CGFloat(Double.pi / 2))
            break
        case UIImageOrientation.right, UIImageOrientation.rightMirrored:
            transform = transform.translatedBy(x: 0, y: src.size.height)
            transform = transform.rotated(by: CGFloat(-Double.pi / 2))
            break
        case UIImageOrientation.up, UIImageOrientation.upMirrored:
            break
        }
        
        switch src.imageOrientation {
        case UIImageOrientation.upMirrored, UIImageOrientation.downMirrored:
            transform.translatedBy(x: src.size.width, y: 0)
            transform.scaledBy(x: -1, y: 1)
            break
        case UIImageOrientation.leftMirrored, UIImageOrientation.rightMirrored:
            transform.translatedBy(x: src.size.height, y: 0)
            transform.scaledBy(x: -1, y: 1)
        case UIImageOrientation.up, UIImageOrientation.down, UIImageOrientation.left, UIImageOrientation.right:
            break
        }
        
        let ctx:CGContext = CGContext(data: nil, width: Int(src.size.width), height: Int(src.size.height), bitsPerComponent: (src.cgImage)!.bitsPerComponent, bytesPerRow: 0, space: (src.cgImage)!.colorSpace!, bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue)!
        
        ctx.concatenate(transform)
        
        switch src.imageOrientation {
        case UIImageOrientation.left, UIImageOrientation.leftMirrored, UIImageOrientation.right, UIImageOrientation.rightMirrored:
            ctx.draw(src.cgImage!, in: CGRect(x: 0, y: 0, width: src.size.height, height: src.size.width))
            break
        default:
            ctx.draw(src.cgImage!, in: CGRect(x: 0, y: 0, width: src.size.width, height: src.size.height))
            break
        }
        
        let cgimg:CGImage = ctx.makeImage()!
        let img:UIImage = UIImage(cgImage: cgimg)
        
        return img
    }
    
    
    func resizeImage(image: UIImage) -> UIImage {
        var actualHeight: Float = Float(image.size.height)
        var actualWidth: Float = Float(image.size.width)
        let maxHeight: Float = Float(AppContstants.maxHeightPhoto)
        let maxWidth: Float = Float(AppContstants.maxWidthPhoto)
        var imgRatio: Float = actualWidth / actualHeight
        let maxRatio: Float = maxWidth / maxHeight
        //50 percent compression
        
        if actualHeight > maxHeight || actualWidth > maxWidth {
            if imgRatio < maxRatio {
                //adjust width according to maxHeight
                imgRatio = maxHeight / actualHeight
                actualWidth = imgRatio * actualWidth
                actualHeight = maxHeight
            }
            else if imgRatio > maxRatio {
                //adjust height according to maxWidth
                imgRatio = maxWidth / actualWidth
                actualHeight = imgRatio * actualHeight
                actualWidth = maxWidth
            }
            else {
                actualHeight = maxHeight
                actualWidth = maxWidth
            }
        }
        
        let rect = CGRect(x: 0, y: 0, width: CGFloat(actualWidth), height: CGFloat(actualHeight))
        UIGraphicsBeginImageContext(rect.size)
        image.draw(in: rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        let imageData = UIImageJPEGRepresentation(img!, AppContstants.compressionQuality)
        UIGraphicsEndImageContext()
        return UIImage(data: imageData!)!
    }
    
    
    @IBAction func unwindToVC1(segue:UIStoryboardSegue) { }
    
}
