//
//  ListTableViewController.swift
//  litter
//
//  Created by Леха Кирон on 26.06.2018.
//  Copyright © 2018 nerox. All rights reserved.
//

import UIKit
import Alamofire


struct ItemLitter {
    let id: Int
    let description: String
    let imagesURL: String
}

struct Image {
    let url: String
}

class ListTableViewController: UITableViewController {
    
    var listLitter: Array<ItemLitter> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let rc = UIRefreshControl()
        rc.addTarget(self, action: #selector(self.refresh(refreshControl:)), for: UIControlEvents.valueChanged)
        tableView.refreshControl = rc
        
        update(nil)
    }
    
    @objc func refresh(refreshControl: UIRefreshControl) {
        update(refreshControl)
    }
    
    func update(_ refreshControl: UIRefreshControl?) {
        Alamofire.request("\(AppContstants.urlApi)/\(AppContstants.versionApi)/litter/list").responseJSON { response in
            if let json = response.result.value {
                if let array = json as? [Any] {
                    self.listLitter = [];
                    for object in array {
                        if let dictionary = object as? [String: Any] {
                            if let id = dictionary["id"] as? Int, let decription = dictionary["description"] as? String, let images = dictionary["images"] as? [String] {
                                var imageURL = "https://i.ytimg.com/vi/CkJxQF1-iiY/maxresdefault.jpg"
                                if(images.count > 0){
                                    imageURL = "\(AppContstants.imageUrlSize1)/\(images.first!)"
                                }
                                
                                let litter = ItemLitter(id: id, description: decription, imagesURL: imageURL);
                                self.listLitter.append(litter)
                            }
                        }
                    }
                }
            }
        
            refreshControl?.endRefreshing()
            self.tableView.reloadData()
        }
    }
    
    
    // Uncomment the following line to preserve selection between presentations
    // self.clearsSelectionOnViewWillAppear = false
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem
    
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return listLitter.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LitterId", for: indexPath)
        cell.textLabel?.text = listLitter[indexPath.row].description
        let url = URL(string: listLitter[indexPath.row].imagesURL)
        let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
        
        cell.imageView?.image = UIImage(data: data!)
        
        // Configure the cell...
        
        return cell
    }
    
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
