//
//  ViewController.swift
//  litter
//
//  Created by Леха Кирон on 27.04.2018.
//  Copyright © 2018 nerox. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var image: UIImage!
    var lat: String!
    var lon: String!
    var course: String!
    var speed: String!
    var altitude: String!
    var horizontal_accuracy: String!
    
    @IBOutlet weak var barButton: UIBarButtonItem!
    
    @IBOutlet weak var myImageView: UIImageView!
    @IBOutlet weak var uploaderProgress: UIProgressView!
    @IBOutlet weak var statusUplodLabel: UILabel!
    @IBOutlet weak var descriptionField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        //        myImageView.contentMode = .scaleAspectFill
        
        myImageView.image = image;
        statusUplodLabel.text = ""
        self.uploaderProgress.progress = 0;
    }
    
    @IBAction func textFieldEnded(_ sender: UITextField) {
        self.view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    @IBAction func sendData(_ sender: UIBarButtonItem) {
        upload()
    }
    
    
    func upload() {
        if let data = UIImageJPEGRepresentation(image!, 1) {
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                ]
            
            let parameters: Parameters = [
                "device_id": UIDevice.current.identifierForVendor!.uuidString,
                "description": descriptionField.text ?? "",
                "lat": lat,
                "lon": lon,
                "course": course,
                "speed": speed,
                "altitude": altitude,
                "horizontal_accuracy": horizontal_accuracy
            ]
            
            self.statusUplodLabel.text = ""
            Alamofire.upload(
                multipartFormData: { multipartFormData in
                    for (key,value) in parameters {
                        multipartFormData.append((value as! String).data(using: .utf8)!, withName: key)
                    }
                    multipartFormData.append(data, withName: "images[]", fileName: "photo.jpg" ,mimeType: "image/jpeg")
            },
                usingThreshold: UInt64.init(),
                to: "\(AppContstants.urlApi)/\(AppContstants.versionApi)/litter/add",
                method: .post,
                headers: headers,
                encodingCompletion: { encodingResult in
                    switch encodingResult {
                    case .success(let upload, _, _):
                        upload.responseJSON { response in
                            debugPrint("response")
                            debugPrint(response)
                            print("Success with JSON: \(String(describing: response.result.value))")
                            if response.response?.statusCode == 200 {
                                debugPrint(response)
                                self.uploaderProgress.progress = 0;
                                self.statusUplodLabel.text = "Успешно"
                                self.descriptionField.text = ""
                                self.myImageView.image = nil
                                
                                
                                self.performSegue(withIdentifier: "unwindSegueToVC1", sender: self)
                                
                            }
                            else {
                                self.statusUplodLabel.text = "Произошла ошибка!"
                                print(response.result)
                            }
                            
                        }
                        upload.uploadProgress { progress in
                            self.uploaderProgress.progress = Float(progress.fractionCompleted)
                            print(progress.fractionCompleted)
                        }
                    case .failure(let encodingError):
                        print(encodingError)
                        self.statusUplodLabel.text = "Произошла ошибка!"
                        
                    }
            }
            )
        }
    }
    
}
