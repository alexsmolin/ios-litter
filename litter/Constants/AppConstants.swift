//
//  AppConstants.swift
//  litter
//
//  Created by Леха Кирон on 26.06.2018.
//  Copyright © 2018 nerox. All rights reserved.
//

import Foundation
import UIKit


struct AppContstants {
    
    //  Минимальная точность GPS для фиксации
    static let minHorizontalAccuracy: Int = 165
    
    //  Сжатие изображения
    static let compressionQuality: CGFloat = 0.6
    
    //  Максимальная высота изображения
    static let maxHeightPhoto: Int = 2800
    
    // Максимальная ширина изображения
    static let maxWidthPhoto: Int = 2800
    
    //  Адресс API сервера
    static let urlApi: String = "http://api.bluu.ru"
    
    static let imageUrlSize1: String = "\(urlApi)/images/100x100"
    
    //  Версия API
    static let  versionApi:String = "v1"
}
